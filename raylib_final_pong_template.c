/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by Alba Roncero Serrano
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"
#include <stdio.h>
#include <stdlib.h>

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    
    /*Color logoColor = BLUE; //sustituido por imagen
    logoColor.a = 0;*/
    Color titleColor = DARKGREEN;
    titleColor.a = 0;
    
    bool fadeIn = true;
    //bool fadeOut = false;
    float alpha = 0;
    float fadeSpeed = 0.01f;
    
    bool difficulty = false;
    bool winner = false;
    bool draw = false;
    bool pause = false;
    bool player2 = false;
    bool credits = false;
    bool endseconds = false;
    int ganador = 0;
    
    Rectangle pantallanegra;
    
    pantallanegra.width = screenWidth;
    pantallanegra.height = screenHeight;
    pantallanegra.x = 0;
    pantallanegra.y = 0;
    
    const int maxVelocity = 15;
    const int minVelocity = 5;
    
    const int velocidady = 8;
    
    /*int playerLife; //usando otras variables abajo
    int enemyLife;*/
    
    
    Rectangle playerRect = { screenWidth/2 - 200, screenHeight/2-200, 100, 20}; //Background Rectangle
    int margin = 5; 
    Rectangle pfillRect = { playerRect.x + margin, playerRect.y + margin, playerRect.width - (2 * margin), playerRect.height - (2 * margin)};
    Rectangle plifeRect = pfillRect;
    
    Color plifeColor = DARKBLUE;
    
    Rectangle enemyRect = { screenWidth/2 + 100, screenHeight/2-200, 100, 20}; //Background Rectangle
    Rectangle efillRect = { enemyRect.x + margin, enemyRect.y + margin, enemyRect.width - (2 * margin), enemyRect.height - (2 * margin)};
    Rectangle elifeRect = efillRect;

    Color elifeColor = DARKBLUE;
    
    int drainLife = 20; //ir quitando vida
    
    
    // NOTE: Here there are some useful variables (should be initialized)
    Rectangle player;
    player.width = 20;
    player.height = 100;
    player.x = 50;
    player.y = screenHeight/2 - player.height/2;
    //int playerSpeedY;
    
    Rectangle enemy;
    enemy.width = 20;
    enemy.height = 100;
    enemy.x = screenWidth - 50 - enemy.width;
    enemy.y = screenHeight/2 - enemy.height/2;
    //int enemySpeedY;
    
    Vector2 ballPosition;
    ballPosition.x = screenWidth/2;
    ballPosition.y = screenHeight/2;
    
    Vector2 ballSpeed;
    ballSpeed.x = minVelocity;
    ballSpeed.y = minVelocity;
    
    int iaLinex = screenWidth/2+250;//línea para la ia
    int iaLinex_diff = screenWidth/2+220;
    
    int ballRadius = 25;
    
    int secondsCounter = 99;
    
    int framesCounter = 0;          // General pourpose frames counter
    
    //int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    //usada variable winner para esto
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    Font fbb = LoadFontEx("resources/fonts/Breaking_Bad.ttf", 200, 0, 255);
    
    Image loguito = LoadImage("resources/logo.png");
    Texture2D texturelogo = LoadTextureFromImage(loguito);
    Image bg = LoadImage("resources/bg_game.png");
    Texture2D texturebg = LoadTextureFromImage(bg);
    Image walter = LoadImage("resources/waltercillo.png");
    Texture2D texturewalter = LoadTextureFromImage(walter);
    Image hank = LoadImage("resources/hankcito.png");
    Texture2D texturehank = LoadTextureFromImage(hank);
    Image gus = LoadImage("resources/gusito.png");
    Texture2D texturegus = LoadTextureFromImage(gus);
    Image saul = LoadImage("resources/saul2.png");
    Texture2D texturesaul = LoadTextureFromImage(saul);

    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    InitAudioDevice();
    Sound rebote = LoadSound("resources/sounds/bounce.wav");
    Sound expl = LoadSound("resources/sounds/explosion.wav");
    Sound pwinner = LoadSound("resources/sounds/winner.mp3");
    Music music = LoadMusicStream("resources/sounds/music.mp3");
    Sound menu = LoadSound("resources/sounds/menu.mp3");
    Sound hanksound = LoadSound("resources/sounds/hank.mp3");
    Sound gussound = LoadSound("resources/sounds/gus.mp3");
    Sound saulsound = LoadSound("resources/sounds/saul.mp3");
    
    PlayMusicStream(music);
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                framesCounter++;
                if (fadeIn){
                    alpha += fadeSpeed;
                    if (alpha >= 1.0f){
                        alpha=1.0f;
                        fadeIn = !fadeIn;
                    }
                }else{
                    alpha -= fadeSpeed;
                    if (alpha <= 0.0f && framesCounter <180){
                        alpha = 0.0f;
                        fadeIn = !fadeIn;
                    }
                }
                if (framesCounter > 300){
					framesCounter = 0;
                    screen = TITLE;
				}
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)                
                if (titleColor.a < 255){
                    titleColor.a++;
                    if (titleColor.a==20){//así a partir de cierta opacidad sonará la música
                        PlaySound(menu);
                    }
                }
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                if (IsKeyPressed(KEY_T)){
                    player2 = !player2;
                    difficulty = false; //no se puede poner más dificultad si son 2
                }
                if (IsKeyPressed(KEY_C)){
                    credits = !credits;
                }
                if (IsKeyPressed(KEY_D)){
                    difficulty = !difficulty;
                    player2 = false;//como en el otro, no se puede jugar 2 con diciultad IA
                }
                if (IsKeyPressed(KEY_ENTER)){
                    screen = GAMEPLAY;
                    StopSound(menu);
                    PlayMusicStream(music); 
                }
                
                framesCounter++;
                
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
                UpdateMusicStream(music); 

                // TODO: Ball movement logic.........................(0.2p)
                //Movimiento pelota
                if (!winner){//si no hay ganador ni pausa se mueve
                    if (!pause){
                        ballPosition.x += ballSpeed.x;
                        ballPosition.y += ballSpeed.y;
                    
                
                // TODO: Player movement logic.......................(0.2p)
                    if (IsKeyDown(KEY_UP)){
                        player.y -= velocidady;
                    }
                    if (IsKeyDown(KEY_DOWN)){
                        player.y += velocidady;
                    }
                    
                    if (player2==true){
                        if (IsKeyDown(KEY_W)){
                            enemy.y -= velocidady;
                        }
                        if (IsKeyDown(KEY_S)){
                            enemy.y += velocidady;
                        }
                    }
                    
                    // TODO: Enemy movement logic (IA)...................(1p)
                    if (difficulty == true){
                        if( ballPosition.x > iaLinex_diff && player2==false){
                            if(ballPosition.y > enemy.y){
                                enemy.y+=velocidady;
                                }
                            if(ballPosition.y < enemy.y){
                                enemy.y-=velocidady;
                                }
                            }
                        }else{
                        if( ballPosition.x > iaLinex && player2==false){
                            if(ballPosition.y > enemy.y){
                                enemy.y+=velocidady;
                                }
                            if(ballPosition.y < enemy.y){
                                enemy.y-=velocidady;
                                }
                            }
                        }
                    }
                }
                
                //limites pared player
                    if(player.y<0){
                        player.y = 0;
                    }        
                    if(player.y > (screenHeight - player.height)){
                        player.y = screenHeight - player.height;
                    }
                //limites pared enemy
                    if(enemy.y<0){
                        enemy.y = 0;
                    }
                    if(enemy.y > (screenHeight - enemy.height)){
                        enemy.y = screenHeight - enemy.height;
                    }
                
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                
                if (CheckCollisionCircleRec(ballPosition, ballRadius, player)){
                if (ballSpeed.x < 0){
                    PlaySound(rebote);
                    if (abs(ballSpeed.x)<maxVelocity){ //Mirar que la velocidad no pase de una determinada
                        ballSpeed.x *=-1.1; //1.1 para aumentar velocidad cada vez que choca
                        ballSpeed.y *=1.1;
                        } else {
                            ballSpeed.x *=-1;
                        }
                    }
                }
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, enemy)){
                        if (ballSpeed.x > 0){//Para que choque bien con la esquina de la pala
                            PlaySound(rebote);
                            if (abs(ballSpeed.x)<maxVelocity){
                                ballSpeed.x *=-1.1; //1.1 para aumentar velocidad cada vez que choca
                                ballSpeed.y *=1.1;
                            } else {
                                ballSpeed.x *=-1;
                            }
                        }
                    }
                
                // TODO: Collision detection (ball-limits) logic.....(1p)

                //COLISION LADO IZQUIERDO
                if ((ballPosition.x > screenWidth - ballRadius)){
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2;
                    ballSpeed.x = -minVelocity;
                    ballSpeed.y = minVelocity;
                    PlaySound(expl);
                    elifeRect.width -= drainLife;
                }else if(ballPosition.x < ballRadius){//COLISION LADO DERECHO
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        ballSpeed.x = minVelocity;
                        ballSpeed.y = minVelocity;
                        plifeRect.width -= drainLife;
                        PlaySound(expl);
                    }
                    
                    //COLISION ENTORNO
                if((ballPosition.y > screenHeight - ballRadius) || (ballPosition.y < ballRadius) ){
                    ballSpeed.y *=-1;
                }
                    
                    // TODO: Life bars decrease logic....................(1p)
                if (elifeRect.width <=0){//gana player
                    elifeRect.width = 0; 
                    ganador = 1;
                    PlaySound(pwinner);
                }else if (elifeRect.width <=efillRect.width/2){
                    elifeColor = SKYBLUE;
                }
                if (plifeRect.width <=0){//gana enemigo
                    plifeRect.width = 0;
                    ganador = 2;
                    PlaySound(hanksound);                            
                }else if (plifeRect.width <=pfillRect.width/2){
                    plifeColor = SKYBLUE;
                }

                // TODO: Time counter logic..........................(0.2p)
                if (!pause){
                    framesCounter++;
                    if (framesCounter >= 60) {
                        framesCounter = 0;
                        secondsCounter--;
                        }

                }
                // TODO: Game ending logic...........................(0.2p)
                if (ganador == 1 || ganador == 2){
                    StopMusicStream(music); 
                    winner = true;
                    screen=ENDING;
                }else if (secondsCounter == 0 && (elifeRect.width!=plifeRect.width)){
                    StopMusicStream(music);
                    endseconds=true;
                    screen=ENDING;
                }else if (secondsCounter == 0 && (elifeRect.width==plifeRect.width)){ //empate
                    StopMusicStream(music);
                    PlaySound(saulsound);
                    draw = true;
                    screen=ENDING;
                }
                
                
                // TODO: Pause button logic..........................(0.2p)
                if (IsKeyPressed(KEY_P)){
                    //framesCounter++;
                    pause = !pause;
                    if (pause){
                        PauseMusicStream(music);
                    }else{
                        ResumeMusicStream(music);
                    }
                    PlaySound(gussound);
                }
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                // TODO: Replay / Exit game logic....................(0.5p)
                if (IsKeyPressed(KEY_ENTER)){
                    //reseteamos variables
                    ganador = 0;
					screen = TITLE;
					framesCounter = 10;
                    winner = false;
                    draw = false;
                    elifeRect = efillRect;
                    plifeRect = pfillRect;
                    elifeColor = DARKBLUE;
                    plifeColor = DARKBLUE;
                    secondsCounter = 99;
                    player.x = 50;
                    player.y = screenHeight/2 - player.height/2;
                    enemy.x = screenWidth - 50 - enemy.width;
                    enemy.y = screenHeight/2 - enemy.height/2;
                    player2 = false;
                    endseconds = false;
                    PlaySound(menu);
                    credits = false;
                    difficulty = false;
				}
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    //DrawText("LOGO", screenWidth/2-120, screenHeight/2-50, 80, Fade(logoColor, alpha));
                    DrawTexture(texturelogo, screenWidth/2 - texturelogo.width/2, screenHeight/2 - texturelogo.height/2, Fade(WHITE, alpha));
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    DrawTextEx(fbb, FormatText("BREAKING"), (Vector2){screenWidth/2-280, screenHeight/2-130}, 100, 0, titleColor);
                    DrawTextEx(fbb, FormatText("PONG"), (Vector2){screenWidth/2-150, screenHeight/2-60}, 100, 0, titleColor);
                    
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if ((framesCounter/30)%2){
                        DrawTextEx(fbb, FormatText("press enter to play"), (Vector2){screenWidth/2-140, screenHeight/2+120}, 60, 0, DARKGREEN);
                    }
                    DrawText("PRESS T FOR SECOND PLAYER", screenWidth/2-100, screenHeight/2+120, 10, GRAY);
                    DrawText("PRESS D FOR MORE DIFFICULTY", screenWidth/2-100, screenHeight/2+130, 10, GRAY);
                    DrawText("PRESS C FOR CREDITS", screenWidth/2-80, screenHeight/2+140, 10, GRAY);
                    
                    if (player2==true){//controles para ambos jugadores
                        DrawText("TWO PLAYERS", screenWidth/2+230, 20, 10, GRAY);
                        DrawText("FIRST PLAYER: KEY UP / KEY DOWN", 30, 30, 10, GRAY);
                        DrawText("SECOND PLAYER: W / S", screenWidth/2+230, 30, 10, GRAY);
                    }
                    if (credits==true){
                        DrawRectangleRec(pantallanegra, (Color){0, 0, 0, 255});
                        DrawTextEx(fbb, FormatText("CREDITS"), (Vector2){screenWidth/2-180, 30}, 70, 0, WHITE);
                        DrawText("PRESS C AGAIN TO EXIT", screenWidth/2+230, 20, 10, GRAY);
                        DrawText("EDITING OF GRAPHICS AND SOUND MADE BY ALBA RONCERO", screenWidth/2-160, screenHeight/2, 10, WHITE);
                        DrawText("ALL CREDITS GO TO BREAKING BAD", screenWidth/2-100, screenHeight/2+10, 10, WHITE);
                        DrawText("PROJECT FOR CEV BARCELONA", screenWidth/2-90, screenHeight/2+20, 10, WHITE);
                        DrawTextEx(fbb, FormatText("LET'S GET COOKING"), (Vector2){screenWidth/2-310, screenHeight/2+130}, 60, 0, WHITE);
                    }
                    if (difficulty==true){//controles para ambos jugadores
                        DrawText("DIFFICULTY ACTIVATED", screenWidth/2+230, 20, 10, GRAY);
                        DrawText("PRESS AGAIN TO DEACTIVATE", screenWidth/2+220, 30, 10, GRAY);
                    }
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    DrawTexture(texturebg, 0, 0, WHITE);
                    DrawCircleV(ballPosition, ballRadius, GREEN);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec(player, DARKBLUE);
                    DrawRectangleRec(enemy, GOLD);
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    DrawRectangleRec (playerRect, BLACK);
                    DrawRectangleRec (pfillRect, MAROON);
                    DrawRectangleRec (plifeRect, plifeColor);
                    
                    DrawRectangleRec (enemyRect, BLACK);
                    DrawRectangleRec (efillRect, MAROON);
                    DrawRectangleRec (elifeRect, elifeColor);
                    
                    //línea para IA
                    if (difficulty == true){
                        DrawLine(iaLinex_diff, 0, iaLinex_diff , screenHeight, Fade(RED, 0.0f)); 
                    }else {DrawLine(iaLinex, 0, iaLinex , screenHeight, Fade(RED, 0.0f)); }
                    
                    // TODO: Draw time counter.......................(0.5p)
                    DrawText(FormatText("%02i", secondsCounter), screenWidth/2-20, 30, 20, BLUE);
                    
                    // TODO: Draw pause message when required........(0.5p)
                    if (pause){
                        //si se pausa, pantalla con menos opacidad, así vemos que no se mueve nada
                        //DrawRectangleRec(pantallanegra, (Color){0, 0, 0, 255-75});
                        DrawTexture(texturegus, 0, 0, WHITE);
                        //escribir texto de pausa
                        DrawTextEx(fbb, FormatText("PAUSE"), (Vector2){screenWidth/2-300, screenHeight/2-100}, 80, 0, BLACK);
                        DrawTextEx(fbb, FormatText("press P to resume the game"), (Vector2){screenWidth/2-300, screenHeight/2+60}, 40, 0, BLACK);
                    }
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    if (ganador == 1){
                        DrawTexture(texturewalter, 0, 0, WHITE);
                        DrawTextEx(fbb, FormatText("I AM THE DANGER"), (Vector2){screenWidth/2-250, screenHeight/2+100}, 50, 0, DARKGREEN);
                        DrawText("PRESS ENTER TO GO TO MENU", screenWidth/2+230, 20, 10, GRAY);
                    }else if (ganador == 2){
                        DrawTexture(texturehank, 0, 0, WHITE);
                        DrawText("PRESS ENTER TO GO TO MENU", screenWidth/2+230, 20, 10, BLACK);
                    }else if (endseconds == true){
                        DrawTextEx(fbb, FormatText("TIME OUT"), (Vector2){screenWidth/2-260, screenHeight/2-75}, 100, 0, DARKGREEN);
                        DrawTextEx(fbb, FormatText("You either run from things, or you face them, Mr. White."), (Vector2){screenWidth/2-275, screenHeight/2+150}, 40, 0, DARKGREEN);
                        DrawText("PRESS ENTER TO GO TO MENU", screenWidth/2+230, 20, 10, BLACK);
                    }else if (draw == true){
                        DrawTexture(texturesaul, 0,0, WHITE);
                        //DrawTextEx(fbb, FormatText("DRAW"), (Vector2){screenWidth/2+175, screenHeight/2}, 60, 0, GOLD);
                        DrawText("PRESS ENTER TO GO TO MENU", screenWidth/2+230, 20, 10, GRAY);
                        DrawText("LET'S START WITH SOME TOUGH LOVE", screenWidth/2+160, screenHeight/2, 10, GRAY);
                        DrawText("YOU TWO SUCK AT PEDDLING METH", screenWidth/2+165, screenHeight/2+10, 10, GRAY);
                        DrawText("PERIOD", screenWidth/2+250, screenHeight/2+20, 10, GRAY);
                    }
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
      

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadFont(fbb);
    UnloadSound(rebote);
    UnloadSound(expl);
    UnloadSound(pwinner);
    UnloadSound(menu);
    UnloadSound(hanksound);
    UnloadSound(gussound);
    UnloadSound(saulsound);
    UnloadMusicStream(music);
    UnloadTexture(texturelogo);
    UnloadTexture(texturebg);
    UnloadTexture(texturewalter);
    UnloadTexture(texturehank);
    UnloadTexture(texturesaul);

    
    CloseAudioDevice(); 
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}